package com.example.basic.repository;

import com.example.basic.entity.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
//    public Department findBydeName(String departmentName);

//    for Case Insensitive Queries
    public Department findBydeNameIgnoreCase(String departmentName);
//    public Department findBydeNameLike(String departmentName);

}
